Contribute
==================================================
Many thanks for contributing to minecraft-launcher-lib! You can do the following thinks:

- You can test minecraft-launcher-lib with all versions and `write a Bug report <https://gitlab.com/JakobDev/minecraft-launcher-lib/-/issues>`_ if something did not work
- You can help developing minecraft-launcher-lib. Check out the :doc:`/develop/index` section of the documentation.
- You can can improve the documentation. See :doc:`/develop/build_and_edit_documentation`.
- You can also update the `Examples <https://gitlab.com/JakobDev/minecraft-launcher-lib/-/tree/master/examples>`_ and add new one.
