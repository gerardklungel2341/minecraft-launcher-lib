microsoft_types
==========================
This module contains all Types for the :doc:`/modules/microsoft_account` module.
It has it's own module because of the many types needed that are not used somewhere else.

.. code:: python

{{types_source}}
